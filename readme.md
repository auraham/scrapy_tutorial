# Scrapy Tutorial



## Creating a project

Create a new project as follows:

```
$ cd ~/git/scrapy_tutorial
$ scrapy startproject tutorial
```

You will see this message:

```
You can start your first spider with:
    cd tutorial
    scrapy genspider example example.com
```



## Starting the crawler

```
$ cd ~/git/scrapy_tutorial
$ scrapy crawl quotes
```

You will see this output:

```
2020-05-16 15:52:43 [scrapy.utils.log] INFO: Scrapy 2.1.0 started (bot: tutorial)
2020-05-16 15:52:43 [scrapy.utils.log] INFO: Versions: lxml 4.5.0.0, libxml2 2.9.10, cssselect 1.1.0, parsel 1.6.0, w3lib 1.22.0, Twisted 20.3.0, Python 3.5.2 (default, Nov 12 2018, 13:43:14) - [GCC 5.4.0 20160609], pyOpenSSL 19.1.0 (OpenSSL 1.1.1g  21 Apr 2020), cryptography 2.9.2, Platform Linux-4.15.0-99-generic-x86_64-with-Ubuntu-16.04-xenial
2020-05-16 15:52:43 [scrapy.utils.log] DEBUG: Using reactor: twisted.internet.epollreactor.EPollReactor
2020-05-16 15:52:43 [scrapy.crawler] INFO: Overridden settings:
{'BOT_NAME': 'tutorial',
 'NEWSPIDER_MODULE': 'tutorial.spiders',
 'ROBOTSTXT_OBEY': True,
 'SPIDER_MODULES': ['tutorial.spiders']}
2020-05-16 15:52:43 [scrapy.extensions.telnet] INFO: Telnet Password: f50b7841d34a413f
2020-05-16 15:52:43 [scrapy.middleware] INFO: Enabled extensions:
['scrapy.extensions.memusage.MemoryUsage',
 'scrapy.extensions.telnet.TelnetConsole',
 'scrapy.extensions.corestats.CoreStats',
 'scrapy.extensions.logstats.LogStats']
2020-05-16 15:52:43 [scrapy.middleware] INFO: Enabled downloader middlewares:
['scrapy.downloadermiddlewares.robotstxt.RobotsTxtMiddleware',
 'scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware',
 'scrapy.downloadermiddlewares.downloadtimeout.DownloadTimeoutMiddleware',
 'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware',
 'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware',
 'scrapy.downloadermiddlewares.retry.RetryMiddleware',
 'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware',
 'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware',
 'scrapy.downloadermiddlewares.redirect.RedirectMiddleware',
 'scrapy.downloadermiddlewares.cookies.CookiesMiddleware',
 'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware',
 'scrapy.downloadermiddlewares.stats.DownloaderStats']
2020-05-16 15:52:43 [scrapy.middleware] INFO: Enabled spider middlewares:
['scrapy.spidermiddlewares.httperror.HttpErrorMiddleware',
 'scrapy.spidermiddlewares.offsite.OffsiteMiddleware',
 'scrapy.spidermiddlewares.referer.RefererMiddleware',
 'scrapy.spidermiddlewares.urllength.UrlLengthMiddleware',
 'scrapy.spidermiddlewares.depth.DepthMiddleware']
2020-05-16 15:52:43 [scrapy.middleware] INFO: Enabled item pipelines:
[]
2020-05-16 15:52:43 [scrapy.core.engine] INFO: Spider opened
2020-05-16 15:52:43 [scrapy.extensions.logstats] INFO: Crawled 0 pages (at 0 pages/min), scraped 0 items (at 0 items/min)
2020-05-16 15:52:43 [scrapy.extensions.telnet] INFO: Telnet console listening on 127.0.0.1:6023
2020-05-16 15:52:44 [scrapy.core.engine] DEBUG: Crawled (404) <GET http://quotes.toscrape.com/robots.txt> (referer: None)
2020-05-16 15:52:44 [scrapy.downloadermiddlewares.redirect] DEBUG: Redirecting (301) to <GET http://quotes.toscrape.com/page/1/> from <GET http://quotes.toscrape.com/page/1>
2020-05-16 15:52:45 [scrapy.downloadermiddlewares.redirect] DEBUG: Redirecting (301) to <GET http://quotes.toscrape.com/page/2/> from <GET http://quotes.toscrape.com/page/2>
2020-05-16 15:52:45 [scrapy.core.engine] DEBUG: Crawled (200) <GET http://quotes.toscrape.com/page/1/> (referer: None)
2020-05-16 15:52:45 [quotes] DEBUG: Saved file quotes-1.html
2020-05-16 15:52:45 [scrapy.core.engine] DEBUG: Crawled (200) <GET http://quotes.toscrape.com/page/2/> (referer: None)
2020-05-16 15:52:45 [quotes] DEBUG: Saved file quotes-2.html
2020-05-16 15:52:45 [scrapy.core.engine] INFO: Closing spider (finished)
2020-05-16 15:52:45 [scrapy.statscollectors] INFO: Dumping Scrapy stats:
{'downloader/request_bytes': 1131,
 'downloader/request_count': 5,
 'downloader/request_method_count/GET': 5,
 'downloader/response_bytes': 7001,
 'downloader/response_count': 5,
 'downloader/response_status_count/200': 2,
 'downloader/response_status_count/301': 2,
 'downloader/response_status_count/404': 1,
 'elapsed_time_seconds': 2.45982,
 'finish_reason': 'finished',
 'finish_time': datetime.datetime(2020, 5, 16, 20, 52, 45, 820487),
 'log_count/DEBUG': 7,
 'log_count/INFO': 10,
 'memusage/max': 55525376,
 'memusage/startup': 55525376,
 'response_received_count': 3,
 'robotstxt/request_count': 1,
 'robotstxt/response_count': 1,
 'robotstxt/response_status_count/404': 1,
 'scheduler/dequeued': 4,
 'scheduler/dequeued/memory': 4,
 'scheduler/enqueued': 4,
 'scheduler/enqueued/memory': 4,
 'start_time': datetime.datetime(2020, 5, 16, 20, 52, 43, 360667)}
2020-05-16 15:52:45 [scrapy.core.engine] INFO: Spider closed (finished)
```

The response files are stored in the root directory:

```
$ cd ~/git/scrapy_tutorial/tutorial
$ ls 
quotes-1.html  quotes-2.html  scrapy.cfg  tutorial
```



## Extracting quotes

You can use this command to interact with requests and responses:

```
scrapy shell 'http://quotes.toscrape.com/page/1/'
```

This will create a `response` object for analyzing the HTML document. You an view this object in your browser:

```
view(response)
```

In addition to `response`, there are other objects generated by `scrapy shell`:

```
[s] Available Scrapy objects:
[s]   scrapy     scrapy module (contains scrapy.Request, scrapy.Selector, etc)
[s]   crawler    <scrapy.crawler.Crawler object at 0x7f57f9e26e80>
[s]   item       {}
[s]   request    <GET http://quotes.toscrape.com/page/1/>
[s]   response   <200 http://quotes.toscrape.com/page/1/>
[s]   settings   <scrapy.settings.Settings object at 0x7f57f3192dd8>
[s]   spider     <DefaultSpider 'default' at 0x7f57f2826390>
[s] Useful shortcuts:
[s]   fetch(url[, redirect=True]) Fetch URL and update local objects (by default, redirects are followed)
[s]   fetch(req)                  Fetch a scrapy.Request and update local objects 
[s]   shelp()           Shell help (print this help)
[s]   view(response)    View response in a browser
```

If you check the HTML document with `view(response)`, you notice that each quote is like this example:

```html
<div class="quote" itemscope itemtype="http://schema.org/CreativeWork">
        <span class="text" itemprop="text">“The world as we have created it is a process of our thinking. It cannot be changed without changing our thinking.”</span>
        <span>by <small class="author" itemprop="author">Albert Einstein</small>
        <a href="/author/Albert-Einstein">(about)</a>
        </span>
        <div class="tags">
            Tags:
            <meta class="keywords" itemprop="keywords" content="change,deep-thoughts,thinking,world" /    > 
            
            <a class="tag" href="/tag/change/page/1/">change</a>
            
            <a class="tag" href="/tag/deep-thoughts/page/1/">deep-thoughts</a>
            
            <a class="tag" href="/tag/thinking/page/1/">thinking</a>
            
            <a class="tag" href="/tag/world/page/1/">world</a>
            
        </div>
    </div>
```

There are two ways for extracting data:

- Using `css`
- Using `xpath`

Let's begin with `css`. You can get the quotes as follows:

```python
response.css("div.quote span.text::text").getall()
```

This is the output:

```python
['“The world as we have created it is a process of our thinking. It cannot be changed without changing our thinking.”',
 '“It is our choices, Harry, that show what we truly are, far more than our abilities.”',
 '“There are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.”',
 '“The person, be it gentleman or lady, who has not pleasure in a good novel, must be intolerably stupid.”',
 "“Imperfection is beauty, madness is genius and it's better to be absolutely ridiculous than absolutely boring.”",
 '“Try not to become a man of success. Rather become a man of value.”',
 '“It is better to be hated for what you are than to be loved for what you are not.”',
 "“I have not failed. I've just found 10,000 ways that won't work.”",
 "“A woman is like a tea bag; you never know how strong it is until it's in hot water.”",
 '“A day without sunshine is like, you know, night.”']
```

We can also get the author names:

```python
response.css("div.quote small.author::text").getall()
```

This is the output:

```python
['Albert Einstein',
 'J.K. Rowling',
 'Albert Einstein',
 'Jane Austen',
 'Marilyn Monroe',
 'Albert Einstein',
 'André Gide',
 'Thomas A. Edison',
 'Eleanor Roosevelt',
 'Steve Martin']
```

In addition to `css`, we could also use `xpath` for getting data. For instance, in order to extract all quotes, you can use this command:

```python
response.xpath("//div[@class='quote']//span[@class='text']/text()").getall() 
```

This is the output, a list of strings:

```python
['“The world as we have created it is a process of our thinking. It cannot be changed without changing our thinking.”',
 '“It is our choices, Harry, that show what we truly are, far more than our abilities.”',
 '“There are only two ways to live your life. One is as though nothing is a miracle. The other is as though everything is a miracle.”',
 '“The person, be it gentleman or lady, who has not pleasure in a good novel, must be intolerably stupid.”',
 "“Imperfection is beauty, madness is genius and it's better to be absolutely ridiculous than absolutely boring.”",
 '“Try not to become a man of success. Rather become a man of value.”',
 '“It is better to be hated for what you are than to be loved for what you are not.”',
 "“I have not failed. I've just found 10,000 ways that won't work.”",
 "“A woman is like a tea bag; you never know how strong it is until it's in hot water.”",
 '“A day without sunshine is like, you know, night.”']
```

Similarly, you can get the author names as follows:

```python
response.xpath("//div[@class='quote']//small[@class='author']/text()").getall() 
```

This is the output:

```python
['Albert Einstein',
 'J.K. Rowling',
 'Albert Einstein',
 'Jane Austen',
 'Marilyn Monroe',
 'Albert Einstein',
 'André Gide',
 'Thomas A. Edison',
 'Eleanor Roosevelt',
 'Steve Martin']
```





## Contact

Auraham Camacho `auraham.cg@gmail.com`