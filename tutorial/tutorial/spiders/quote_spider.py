# quote_spider.py
import scrapy

class QuotesSpider(scrapy.Spider):
    name = "quotes"         # identifies the spider and must be unique within a project
    
    def start_requests(self):
        """
        Returns an iterable (generator) of Requests or a list of Request
        """
        
        urls = (
            "http://quotes.toscrape.com/page/1",
            "http://quotes.toscrape.com/page/2",
            )
        
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
            
    def parse(self, response):
        """
        Handles the response downloaded for each request
        
        Input
        response        TextResponse
        """
        page = response.url.split("/")[-2]      # url = http://quotes.toscrape.com/page/2/
        filename = "quotes-%s.html" % page
        with open(filename, "wb") as f:
            f.write(response.body)
        self.log("Saved file %s" % filename)
        
